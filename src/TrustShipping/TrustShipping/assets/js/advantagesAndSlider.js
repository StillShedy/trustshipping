const $video = document.getElementById('video');
const $play = document.getElementById('advantages');
var check = true;
const playVideo = () => {
    if (check) {
            $video.play();
            check = false;
    } else {
            $video.pause();
            check = true;
    }
            
};

$play.addEventListener('click', playVideo);


$(".content_advantages_mobile").slick({
    dots: true,
    infinite: true,
    arrows: false,
    slidesToShow: 1,
    slidesToScroll: 1
});


function scrollToElement(target) {
    var element = document.getElementById(target);
    var rect = element.offsetTop - 110;
    $([document.documentElement, document.body]).animate({
        scrollTop: rect
    }, 500);
}