﻿let currentIndex = -1;
$(document).ready(function () {

    //E-mail Ajax Send
    $(".form-contact-car-slider").submit(function () { //Change

        $('.send-phone-request').prop('disabled', true);
        var th = {
            number: $("#tel-request-form").val(),
            name: $("#name-request-form").val(),
            auto: $("#car-request-form").val()
        }

        $.ajax({
            type: "POST",
            url: "/umbraco/surface/Mail/SendMail", //Change
            data: th

        }).done(function () {
            $('.send-phone-request').prop('disabled', false);
            location.href = '/#car-carousel';
        }).fail(function (data) {
            $('.send-phone-request').prop('disabled', false);
            console.log(data);
        });

        return false;
    });



});



$('.slider-pictures').slick({
    arrows: true,
    dots: false,
    infinite: false,
    speed: 300,
    slidesToShow: 1,
    slidesToScroll: 1,
    variableWidth: true,
    draggable: false,
    swipe: true,
    centerMode: true,
    centerPadding: '120px',
    initialSlide: 2,
    variableWidth: true,
    responsive: [
        {
            breakpoint: 1024,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 3,
                arrows: true,
                dots: false,
                swipe: true
            }
        },
        {
            breakpoint: 600,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 2,
                arrows: true,
                dots: false,
                swipe: true
            }
        },
        {
            breakpoint: 480,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: true,
                dots: false,
                swipe: true,
                centerMode: true,
                variableWidth: true
            }
        }
    ]
});

$('.slider-pictures').on('click', '.slick-prev', function (e) {
    if (currentIndex > -1) {
        var tempIndex = -1;
        $(".carousel-auto").each(function (index) {
            var current = $($(".carousel-auto")[index]);
            if (currentIndex > current.attr('data-slick-index') && current.is(':visible')) {
                tempIndex = current.attr('data-slick-index');
            }
        })
        if (tempIndex > -1) {
            currentIndex = tempIndex;
        }
        setTimeout(function () {
            $('.slider-pictures').slick('slickGoTo', Number(currentIndex));
        }, 500)

    }
})

$('.slider-pictures').on('click', '.slick-next', function (e) {
    if (currentIndex > -1) {
        var found = false;
        $(".carousel-auto").each(function (index) {
            var current = $($(".carousel-auto")[index]);
            if (currentIndex < current.attr('data-slick-index') && current.is(':visible')) {
                if (found == false) {
                    currentIndex = current.attr('data-slick-index');
                    found = true;
                }
            }
        })

        setTimeout(function () {
            $('.slider-pictures').slick('slickGoTo', Number(currentIndex));
        }, 500)
    }
})

const getCars = function () {
    function renewCarInRangePrice(leftBorder, rightBorder) {
        var index = 0;
        var indexToGo = 0;
        $(".carousel-auto").each((el) => {
            var element = $($(".carousel-auto")[el]);
            var price = element.attr("data-price");
            element.removeClass("slick-current slick-center");

            if (price >= leftBorder && price <= rightBorder) {
                if (index === 0) {
                    indexToGo = element.attr("data-slick-index");
                }

                if (index === 0 || index === 1) {
                    element.attr('tabindex', 0);
                }
                else {
                    element.attr('tabindex', -1);
                }
                index++;
                element.show();
            }
            else {
                element.removeAttr("tabindex");
                element.hide();
            }
        })

        currentIndex = indexToGo;
        $('.slider-pictures').slick('slickGoTo', indexToGo);
    };

    return {
        changeRange: (val1, val2) => {
            let leftValue = val1;
            let rightValue = val2;
            if (val1 > val2) {
                leftValue = val2;
                rightValue = val1;
            }
            renewCarInRangePrice(leftValue, rightValue);
        }
    };



};
let leftTitle = 7;
let rightTitle = 40;
$(document).ready(function () {
    $('#slider').slider({

        min: 7,

        max: 40,

        step: 1,

        range: 'true',

        values: [7, 40],

        slide: function (e, ui) {
            var index = ui.values.indexOf(ui.value);
            ui.handle.innerHTML = `<span data-value="${ui.value}" id=${e.target.getAttribute("id")} class="ts-slider-range-value ts-slide-position-${index}">` + ui.value + 'K' + '</span>';

        },

        create: function (event, ui) {

            var ui_sl_handles = $(this).find('.ui-slider-handle');

            ui_sl_handles[0].innerHTML = '<span data-value="7" id="range-border-one" class="slider-range-value ts-left-position min-val">' + leftTitle + 'K</span>';

            ui_sl_handles[1].innerHTML = '<span data-value="4" id="range-border-two" class="slider-range-value ts-right-position max-val">' + rightTitle + 'K</span>';

            $('#in-budget').attr('value', '7-40');

        },

        change: function (event, ui) {
            $('#in-budget').attr('value', ui.values[0] + '-' + ui.values[1]);

            getCars().changeRange(ui.values[0], ui.values[1]);

        }
    }
    );
});


$(".pop-up-button").on('click', function () {
    $(".custom-model-main").addClass('model-open');
    $(".ui-title-inner").removeClass("none-disp");
    $(".ui-title-inner").addClass("block-disp");
    $(".form-contact-car-slider").removeClass("none-disp");
    $(".form-contact-car-slider").addClass('block-disp');
    $(".thanks-block").removeClass("block-disp");
    $(".thanks-block").addClass("none-disp");
});
$(".close-btn, .bg-overlay").click(function () {
    $(".custom-model-main").removeClass('model-open');
});


$(".send-phone-request").on('click', function () {
    $(".ui-title-inner").removeClass("block-disp");
    $(".ui-title-inner").addClass("none-disp");
    $(".form-contact-car-slider").removeClass("block-disp");
    $(".form-contact-car-slider").addClass('none-disp');
    $(".thanks-block").removeClass("none-disp");
    $(".thanks-block").addClass("block-disp");
});
