﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Threading;
using System.Web;

namespace TrustShipping.Helpers
{
    public class MailHelper
    {

        public void SendLeadToCRMAsync(string to, string text)
        {
            try
            {
                MailMessage message = new MailMessage("trustshipping.com.ua@gmail.com", to);
                message.Subject = "Лид с сайта";
                message.Body = text;
                message.IsBodyHtml = true;
                using (SmtpClient client = new SmtpClient())
                {
                    client.EnableSsl = true;
                    client.Send(message);
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public void SendErrorAsync(Exception error)
        {
            try
            {
                MailMessage message = new MailMessage("trustshipping.com.ua@gmail.com", "trustshipping.com.ua@gmail.com");
                message.Subject = error.Message;
                message.Body = $"{JsonConvert.SerializeObject(error)}";
                using (SmtpClient client = new SmtpClient())
                {
                    client.EnableSsl = true;
                    client.Send(message);
                }
            }
            catch (Exception e)
            {
                throw e;
            }

        }

        public void SendErrorAsync(string error)
        {
            try
            {
                MailMessage message = new MailMessage("trustshipping.com.ua@gmail.com", "trustshipping.com.ua@gmail.com");
                message.Subject = "Debug";
                message.IsBodyHtml = true;
                message.Body = $"{error}";
                using (SmtpClient client = new SmtpClient())
                {
                    client.EnableSsl = true;
                    client.Send(message);
                }
            }
            catch (Exception e)
            {
                throw e;
            }

        }
    }
}