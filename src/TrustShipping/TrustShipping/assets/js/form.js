﻿$(function () {
    $('a[href*=#]:not([href=#])').click(function () {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html,body').animate({
                    scrollTop: target.offset().top
                }, 1000);
                return false;
            }
        }
    });
});


$(document).ready(function () {

    $.get('/Umbraco/Api/GoogleFeedback/GetApplicationFeedbackAsync').done(function (data) {
        var reviews = data.result.reviews;
        var template = $("div[data-id='template']");
        $('.about_reviews').append('<p>' + reviews.length + ' отзывов</p>')
        if (Number.isInteger(data.result.rating)) {
            $('.stars_rate').append('<h1>' + data.result.rating + '.0 </h1>')
        }
        else {
            $('.stars_rate').append('<h1>' + data.result.rating + ' </h1>')
        }

        for (var e = 0; e < data.result.rating; e++) {
            $('.star').append('<img src="/assets/media/reviews/star.png" alt="star" />');
        }


        for (var i = 0; i < reviews.length; i++) {
            var clone = template.clone();
            var item = reviews[i];
            clone.find('.review_author').attr("src", item.profile_photo_url);
            clone.find('.name_author').text(item.author_name);
            for (var e = 0; e < item.rating; e++) {
                clone.find('.star_time').append('<img class="star_author" src="/assets/media/reviews/star.png" alt="star">');
            }

            clone.find('.time_author').append('<p class="time_author">' + item.relative_time_description +'</p>');
            clone.find('.content_author').text(item.text);

            $('.scroll_block').append(clone);
            clone.show();
            $('.scroll_block').append("<hr />")
        }


    })

})
