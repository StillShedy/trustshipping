﻿using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web.Http;
using Umbraco.Web.WebApi;

namespace TrustShipping.Controllers.Api
{
    public class GoogleFeedbackController : UmbracoApiController
    {
        private const string GoogleFeedbackApi = "https://maps.googleapis.com/maps/api/place/details/json?place_id=ChIJofOp6TihJ0ERcGkKgiJ3FT8&fields=name,rating,reviews,formatted_phone_number&language=ru&key=AIzaSyC2_u5QUKM2jZn06ooYdxaFzMJ7NugbRB4";

        [HttpGet]
        public async Task<object> GetApplicationFeedbackAsync()
        {
            using (var httpClient = new HttpClient())
            {
                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                var request = new HttpRequestMessage(HttpMethod.Get, GoogleFeedbackApi);
                return await httpClient.SendAsync(request);
            }
        }
    }
}
