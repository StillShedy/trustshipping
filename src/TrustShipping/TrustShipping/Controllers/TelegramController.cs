﻿using Newtonsoft.Json;
using System;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web.Mvc;
using Telegram.Bot;
using Telegram.Bot.Types;
using TrustShipping.Helpers;
using Umbraco.Core.Services;
using Umbraco.Web;
using Umbraco.Web.Mvc;
using Umbraco.Web.WebApi;

namespace TrustShipping.Controllers
{
    public class TelegramController : SurfaceController
    {
        const string userClientToken = "1464014682:AAHS_DJ6ZUV1Qmzaof0LyP2JJZWuCHPFsOk"; // прием сообщений
        const string managerClientToken = "1203987247:AAEhtiAN_uDYDRBHZu2-M5L2C9BoVOLKEH0"; // бот для ответов

        private readonly IContentService _contentService;
        private readonly MailHelper mailHelper;
        public TelegramController(IContentService contentService)
        {
            _contentService = contentService;
            mailHelper = new MailHelper();
        }

        [HttpGet]
        public async Task StartBot()
        {

            try
            {
                TelegramBotClient userClient = new TelegramBotClient(userClientToken);
                TelegramBotClient managerClient = new TelegramBotClient(managerClientToken);
                await userClient.SetWebhookAsync("https://trustshipping.com.ua/Umbraco/Surface/Telegram/UserAsync");
                await managerClient.SetWebhookAsync("https://trustshipping.com.ua/Umbraco/Surface/Telegram/ManagerAsync");
            }
            catch (Exception e)
            {
                mailHelper.SendErrorAsync(e);
            }
        }

        [HttpPost]
        public async Task UserAsync(Update update)
        {
            var debug = string.Empty;
            try
            {
                var req = Request.InputStream; //get Request from telegram 
                Request.InputStream.Position = 0;
                var responsString = new StreamReader(req).ReadToEnd(); //read request
                RootObject ro = JsonConvert.DeserializeObject<RootObject>(responsString);
                debug += JsonConvert.SerializeObject(ro, Formatting.Indented);


                var settings = Umbraco.ContentAtRoot().FirstOrDefault(x => x.IsDocumentType("telegram"));
                if (settings == null)
                {
                    throw new System.Exception("Конфигурация не создана");
                }

                var typedSettins = new Umbraco.Web.PublishedModels.Telegram(settings);
                var clientsFolder = settings
                    .Children
                    .FirstOrDefault(x => x.IsDocumentType("clients"));
                if (clientsFolder == null)
                {
                    var content = _contentService.Create("Clients", typedSettins.Id, "clients");
                    _contentService.SaveAndPublish(content);
                }

                clientsFolder = settings
                    .Children
                    .FirstOrDefault(x => x.IsDocumentType("clients"));
                if (ro?.message?.chat?.id != null)
                {
                    if (!clientsFolder.Children.Any(x => new Umbraco.Web.PublishedModels.Client(x).Token == ro.message.chat.id.ToString()))
                    {
                        var name = ro.message?.from?.first_name ?? $"{DateTime.Now}";
                        var name2 = ro.message?.from?.last_name ?? $"{DateTime.Now}";
                        var clientId = ro?.message?.from?.username;
                        var token = ro.message.chat.id.ToString();
                        var tgClient = _contentService.Create($"{name}_{name2}", clientsFolder.Id, "client");
                        tgClient.SetValue("tgName", $"{name}_{name2}");
                        tgClient.SetValue("clientId", clientId);
                        tgClient.SetValue("token", token);
                        _contentService.SaveAndPublish(tgClient);
                    }

                }

                TelegramBotClient userClient = new TelegramBotClient(userClientToken);
                TelegramBotClient managerClient = new TelegramBotClient(managerClientToken);

                if (!string.IsNullOrEmpty(update?.Message?.Text) && update.Message.Text == "/start")
                {
                    debug += " /start message ";

                    await userClient.SendPhotoAsync(
                       chatId: update.Message.Chat.Id,
                      photo: typedSettins.Image != null ? "https://trustshipping.com.ua" + typedSettins.Image.Url() : string.Empty,
                      caption: typedSettins.HelloText);
                }
                else
                {
                    debug += " Not Start Message";
                    var chatId = long.Parse(typedSettins.ChatId);
                    if (ro.message?.chat?.id != null && ro.message.chat.id != chatId)
                    {
                        debug += $" Custom User Message. Notification to Chat {ro.message.chat.id}";

                        string userName = "";
                        if (ro?.message?.from?.first_name != null)
                        {
                            userName += ro.message.from.first_name + " ";
                        }
                        if (ro?.message?.from?.last_name != null)
                        {
                            userName += ro.message.from.last_name + " ";
                        }
                        userName += "(@" + update.Message.From.Username + ")";
                        debug += $" before null text check";
                        if (!string.IsNullOrEmpty(update?.Message?.Text)
                            && update.Message.Text != typedSettins.ThanksText
                            && update.Message.Text != typedSettins.HelloText)
                        {
                            debug += $" {update.Message.Text}";
                            await userClient.SendTextMessageAsync(
                       chatId: chatId,
                      text: $"{ro.message.chat.id}. Пользователь {userName} Оставил сообщение: \n {update.Message.Text}");
                            await userClient.SendTextMessageAsync(
                           chatId: ro.message.chat.id,
                          text: typedSettins.ThanksText);
                        }
                    }
                    else
                    {
                        debug += " Message from dialog Message. Notification to user";
                        if (!string.IsNullOrEmpty(ro?.message.reply_to_message?.text))
                        {
                            debug += " ReplyMessage not nul. Notification to user";

                            var text = ro?.message.reply_to_message?.text;
                            var id = text.Split('.').FirstOrDefault();
                            debug += $" message {text}";
                            debug += " userId=" + id.ToString();
                            debug += " chanelText=" + ro?.message?.text;
                            if (!string.IsNullOrEmpty(id))
                            {
                                await userClient.SendTextMessageAsync(
                                chatId: id,
                               text: ro?.message?.text);
                            }
                            
                        }
                        else
                        {
                            debug += " Reply Message Null";
                        }
                    }
                }

                if (typedSettins.EnableDebug)
                {
                    mailHelper.SendErrorAsync(debug);
                }
            }
            catch (Exception e)
            {
                mailHelper.SendErrorAsync(e);
                mailHelper.SendErrorAsync(debug);
            }
        }
    }
}
