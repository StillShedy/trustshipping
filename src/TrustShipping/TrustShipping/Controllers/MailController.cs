﻿using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web.Mvc;
using Telegram.Bot;
using TrustShipping.Helpers;
using Umbraco.Web;
using Umbraco.Web.Mvc;

namespace TrustShipping.Controller
{
    public class MailController : SurfaceController
    {
        const string userClientToken = "1464014682:AAHS_DJ6ZUV1Qmzaof0LyP2JJZWuCHPFsOk"; // прием сообщений

        [HttpPost]
        public void SendMail(string Name, string Number, string Auto)
        {
            try
            {
                var convertedNumber = Number
                    .Replace("(", string.Empty)
                    .Replace(")", string.Empty)
                    .Replace("-", string.Empty)
                    .Replace(" ", string.Empty);
                var text = $"Телефон: {convertedNumber} <br/><br/> Имя: {Name} <br/><br/> Авто: {Auto} <br/><br/> Кузов: Любой <br/><br/> Год: Любой <br/><br/> Бюджет: Любой";
                MailHelper mail = new MailHelper();
                mail.SendLeadToCRMAsync("29151631.185471@parser.amocrm.ru", text);
            }
            catch (System.Exception e)
            {
                var a = e;
            }

        }

        [HttpPost]
        public void SaveForm(Podbor podbor)
        {
            try
            {
                var convertedNumber = podbor.Number
                    .Replace("(", string.Empty)
                    .Replace(")", string.Empty)
                    .Replace("-", string.Empty)
                    .Replace(" ", string.Empty);
                var text = $"Телефон: {convertedNumber} <br/><br/> Имя: {podbor.Name} <br/><br/> Авто: Любой <br/><br/> Кузов: {podbor.Types} <br/><br/> Год: {podbor.Year} <br/><br/> Бюджет: {podbor.Budget}";
                MailHelper mail = new MailHelper();
                mail.SendLeadToCRMAsync("29151631.185471@parser.amocrm.ru", text);
            }
            catch (System.Exception e)
            {
                var a = e;
            }
        }
    }

    public class Podbor
    {
        public string Number { get; set; }
        public string Name { get; set; }
        public string Year { get; set; }
        public string Budget { get; set; }
        public string Types { get; set; }
    }
}
