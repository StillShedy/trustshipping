﻿$(document).ready(function () {

    //E-mail Ajax Send
    $(".form-contact").submit(function () { //Change

        $('#contact-us-form-button').prop('disabled', true);

        console.log()
        var th = {
            number: $("#contact-us-number").val(),
            name: $("#contact-us-name").val(),
            auto: $("#contact-us-auto").val()
        }

        $.ajax({
            type: "POST",
            url: "/umbraco/surface/Mail/SendMail", //Change
            data: th

        }).done(function () {
            $('#contact-us-form-button').prop('disabled', false);
            location.href = '/thank-page';
        }).fail(function (data) {
            $('#contact-us-form-button').prop('disabled', false);
            console.log(data);
        });

        return false;
    });

});